import jinja2
import os
import datetime
import time
import requests
import urlparse
import json
import urllib
import jinja2
from jinja2 import Environment, PackageLoader
from PIL import Image
from trollop import TrelloConnection

#connect up to trello
key = 'f40152f1fdf548455a62e8d6053901b9'
secret = '970ace72ef80c337ff9b4b1ba6f1c37780fb20e898ef3f36a93d0e524765d160'
conn = TrelloConnection(key, secret)
conn.me
conn.me.username

#find what board to use
def boardChoice(busUnit):
	if busUnit == ".":
		board = conn.get_board('56d0602d1283d76f95969650')
	else:
		print "we can't help you."
	return board
		
busUnit = raw_input("Please enter Campaign Board: ")
print busUnit
board = boardChoice(busUnit)

# GET ALL CARDS FROM THE TO CODE TRELLO BOARD
totalCards = len(board.lists[1].cards)
for mycard in board.lists[1].cards:
	print
	print '======='
	print
	campaign = mycard.name
	print 'Description: '
	print mycard.desc
	print
	print
	print '======='
	
	# DOWNLAOD ATTACHMENTS FORM EACH CARD
	cid = mycard._id
	camcard = len(board.lists[1].cards)
	for attached in conn.get_card(cid).attachments[camcard].url:
		while camcard <= len(board.lists[1].cards):
			print "working on card " + str(camcard) + " of " + str(len(board.lists[1].cards))
			xcard = conn.get_card(cid).attachments[camcard].url
			path = urlparse.urlparse(xcard).path
			ext = os.path.splitext(path)[1]
			if ext == ".jpg":
				urllib.urlretrieve(xcard, campaign + ".jpg")
			else:
				urllib.urlretrieve(xcard, campaign + ".csv")
		
			#get some campaign details
			campaignId = campaign
			sendDate = int(raw_input("Send Date (YYYYMMDD): "))
			xheight = int(raw_input("How many pixels down does the body extend? "))

			creative = Image.open( campaignId + '.jpg')

			greybarcrop =  ( 0, 0, 600, 33)
			bodycrop = (0, 107, 600, xheight)
			mykcrop = (0, xheight, 600, (xheight + 163))

			greybar = creative.crop (greybarcrop)
			body = creative.crop (bodycrop)
			mykerastase = creative.crop (mykcrop)

			#set up an output folder
			newpath = r'/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/catfish/Kerastase/' + str(campaignId) 
			if not os.path.exists(newpath):
			    os.makedirs(newpath)

			#save some images
			greybar.save( newpath + '/' + campaignId + 'grey.jpg')
			body.save( newpath + '/' + campaignId + 'body.jpg')
			mykerastase.save( newpath + '/' + campaignId + 'myk.jpg')

			templateLoader = jinja2.FileSystemLoader( searchpath="/" )
			templateEnv = jinja2.Environment( loader=templateLoader )
			TEMPLATE_FILE = "/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/catfish/Kerastase/kcont.html"
			template = templateEnv.get_template( TEMPLATE_FILE )
			templateVars = { "slice1" : campaignId + 'body.jpg'}
			outputText = template.render( templateVars )

			outHTML = newpath + '/' +  str(campaignId) + str(sendDate) + ".html"

			with open(outHTML, "wb") as fh:
			    fh.write(outputText)

			print '***'
			print '***'
			print
			camcard+=1
	



#get some email body details
bodyrows = int(raw_input("How many rows are in the body content? "))
listOfRows = []
rows = 1
totalrowheight = 0
while (rows <= bodyrows):
	rowheight = int(raw_input("How tall is row " + str(rows) + "? "))
	addcols = int(raw_input("How many columns are in row " + str(rows) + " (1, 2 or 3)? "))
	row = []
	if addcols == 1:
		row.append(1)
		row.append(rowheight)
		colwidth = 600
		row.append(colwidth)
		listOfRows.append(row)
	elif addcols == 2:
		row.append(2)
		row.append(rowheight)
		colwidth = 300
		row.append(colwidth)
		listOfRows.append(row)
	elif addcols == 3:		
		row.append(3)
		row.append(rowheight)
		colwidth = 200
		row.append(colwidth)
		listOfRows.append(row)
	else:
		print "please try again"
		break
	rows +=1
	xheight = xheight + rowheight

#do some stuff with those details like start with opening the file
creative = Image.open( campaignId + '.jpg')

print "You have " + str(bodyrows) + " rows in your body with the following details:"
print listOfRows
print "Your body height is: " + str(xheight)