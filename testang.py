import jinja2
from jinja2 import Environment, PackageLoader
from PIL import Image
import os

print '1. Kerastase\n2. Shu\n3. Skin Ceuticles\n4. La Roche Posey\n5. Viche\n6. Dermablend\n7. The Body Shop US\n\n'

busUnit = int(raw_input("Please enter Business Unit: "))

if busUnit == 1:

	campaignId = raw_input("Please enter Campaign Number: ")
	sendDate = int(raw_input("Campaign Send Date (YYYYMMDD): "))
	xheight = int(raw_input("How tall is the body content? "))
	bodyrows = int(raw_input("How many rows are in the body content? "))

	creative = Image.open( campaignId + '.jpg')

	greybarcrop =  ( 0, 0, 600, 33)
	bodycrop = (0, 107, 600, xheight + 107)
	mykcrop = (0, xheight, 600, (xheight + 270))

	greybar = creative.crop (greybarcrop)
	mykerastase = creative.crop (mykcrop)

	#set up an output folder
	newpath = r'/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/Kerastase/' + str(campaignId) 
	if not os.path.exists(newpath):
	    os.makedirs(newpath)

	greybar.save( newpath + '/' + campaignId + 'grey.jpg')

	if bodyrows == 1:
		body = creative.crop (bodycrop)
		body.save( newpath + '/' + campaignId + 'body.jpg')

	bslicerows = 0
	while bslicerows <= bodyrows:
		heightrow = int(raw_input("How tall is row " + bslicerows + '? '))
		body = creative.crop (bodycrop)
		body.save( newpath + '/' + campaignId + 'body' + bslicerows + '.jpg')

	rowtotal = 0
	#loop through the body content rows
	while rowtotal <= bodyrows:
		
	
	mykerastase.save( newpath + '/' + campaignId + 'myk.jpg')

	templateLoader = jinja2.FileSystemLoader( searchpath="/" )
	templateEnv = jinja2.Environment( loader=templateLoader )
	TEMPLATE_FILE = "/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/Kerastase/kcont.html"
	template = templateEnv.get_template( TEMPLATE_FILE )
	templateVars = { "slice1" : campaignId + 'body.jpg'}



	outputText = template.render( templateVars )

	outHTML = newpath + '/' +  str(campaignId) + str(sendDate) + ".html"

	with open(outHTML, "wb") as fh:
	    fh.write(outputText)

	print 'Thank you for using Catfish, your files have been created. Adios! \n'
else:
	print 'Ok, that\'s awkward, Catfish acutally doesn\'t currently support that busines unit: error404 \n'