import jinja2
import os
import csv
import datetime
import time
import requests
import urlparse
import json
import urllib
import jinja2
from jinja2 import Environment, PackageLoader
from PIL import Image
from trollop import TrelloConnection

#set path of catfish on local folder
mypath = "/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/Kerastase/"

#connect up to trello
key = 'f40152f1fdf548455a62e8d6053901b9'
secret = '970ace72ef80c337ff9b4b1ba6f1c37780fb20e898ef3f36a93d0e524765d160'
conn = TrelloConnection(key, secret)
conn.me
conn.me.username

#define brand
print '1. Kerastase\n2. Shu\n3. Skin Ceuticles\n4. La Roche Posey\n5. Viche\n6. Dermablend\n7. The Body Shop US\n -- 0. for test baord -- \n'
def boardChoice(busUnit):
	if busUnit == "0":
		board = conn.get_board('56d0602d1283d76f95969650')
		brand = "test"
		
	elif busUnit == "1":
		board = conn.get_board('56d0602d1283d76f95969650')
		brand = "Kerastase"
		
	else:
		print "we can't help you."

	print "Connecting to the " + brand + " board..."
	return board
		
busUnit = raw_input("Please enter Campaign Board: ")
print busUnit
board = boardChoice(busUnit)


# GET ALL CARDS FROM THE TO CODE TRELLO BOARD
totalCards = len(board.lists[1].cards)
for mycard in board.lists[1].cards:
	print
	print '======='
	print
	campaign = mycard.name
	print campaign
	print "-------"
	print 'Description: '
	print mycard.desc
	print
	print
	print '======='
	print
	print
	
	runnit = int(raw_input("Run this card? 1)Yes 2)No:"))
	if runnit == 1:
		# DOWNLOAD ATTACHMENTS FORM CARD
		cid = mycard._id
		att=0
		for attached in conn.get_card(cid).attachments:
			
			xcard = conn.get_card(cid).attachments[att].url
			path = urlparse.urlparse(xcard).path
			ext = os.path.splitext(path)[1]
			if ext == ".jpg":
				urllib.urlretrieve(xcard, campaign + ".jpg")
				fname = campaign + ".jpg"
			else:
				urllib.urlretrieve(xcard, campaign + ".csv")
				fname = campaign + ".csv"

			att+=1
			print "downloaded file #" + str(att) + " > > > " + fname

		sendDate = conn.get_card(cid).badges['due']
		
		# OPEN THE DOWNLOADED CAMPAIGN JPEG
		img = Image.open(campaign + ".jpg")
		img.show()



		# SLICE UP THAT BAD BOY
		bodyrows = int(raw_input("How many rows are in the body content? "))
		listOfRows = []
		rows = 1
		totalrowheight = 0

		while (rows <= bodyrows):
			rowheight = int(raw_input("How tall is row " + str(rows) + "? "))
			addcols = int(raw_input("How many columns are in row " + str(rows) + " (1, 2 or 3)? "))
			row = []
			if addcols == 1:
				row.append(1)
				row.append(rowheight)
				colwidth = 600
				row.append(colwidth)
				listOfRows.append(row)
			elif addcols == 2:
				row.append(2)
				row.append(rowheight)
				colwidth = 300
				row.append(colwidth)
				listOfRows.append(row)
			elif addcols == 3:		
				row.append(3)
				row.append(rowheight)
				colwidth = 200
				row.append(colwidth)
				listOfRows.append(row)
			else:
				print "please try again"
				break
			rows +=1
			totalrowheight = totalrowheight + rowheight

		#set up an output folder
		print "Creatign campaign folder..."
		newpath = r'/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/catfish/Kerastase/' + str(campaign) 
		if not os.path.exists(newpath):
		    os.makedirs(newpath)

		#slice up some images
		print "Creating image slices... for " + busUnit

		if busUnit == "1":
			creative = Image.open( campaign + '.jpg')
			greybarcrop =  ( 0, 0, 600, 33)
			footImg = (0, totalrowheight, 600, (totalrowheight + 163))
			#slice for row(s)
			if bodyrows > 1:
				bodycrop = (0, 107, 600, totalrowheight + 107)
			else:
				bodycrop = (0, 107, 600, totalrowheight + 107)

		#save some images
		body = creative.crop (bodycrop)
		greybar = creative.crop (greybarcrop)
		footerImg = creative.crop (footImg)
		greybar.save( newpath + '/' + campaign + 'grey.jpg')
		
		#save row(s)
		if bodyrows > 1:
			body.save( newpath + '/' + campaign + 'body.jpg')
		else:
			body.save( newpath + '/' + campaign + 'body.jpg')
			
		footerImg.save( newpath + '/' + campaign + 'myk.jpg')

		templateLoader = jinja2.FileSystemLoader( searchpath="/" )
		templateEnv = jinja2.Environment( loader=templateLoader )
		TEMPLATE_FILE = "/users/justinjohns/Documents/Clients+Work/TASK_FORCE_UNICORN/Figment/catfish/Kerastase/kcont.html"
		template = templateEnv.get_template( TEMPLATE_FILE )
		templateVars = { "slice1" : campaign + 'body.jpg'}
		outputText = template.render( templateVars )

		outHTML = newpath + '/' +  str(campaign) + str(sendDate) + ".html"

		print "writing html file..."
		with open(outHTML, "wb") as fh:
		    fh.write(outputText)

		print '***'
		print '***'
		print

print
print "Thank you for using project catfish. Goodbye."
print

print "---------------------------"